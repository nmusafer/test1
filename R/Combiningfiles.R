#'Combiningfiles
#'
#' This function allows you combine the data in all the files
#' @param files list of file names
#' @return return combined data from the files

#' Combiningfiles()
#'
#'
#' @export

Combiningfiles <- function(files){

  data <- NULL

  for(i in 1:length(files)){

    dt <- CoxPHMModel::readtables(files[i])

    dt[, 1:ncol(dt)] <- lapply( dt[, 1:ncol(dt)], function(x) as.character(x))

    dt$file <- files[i]

    data <- bind_rows(data, dt)


  }

  return(data)

}
