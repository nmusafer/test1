#'PHMModelFitting
#'
#' This function allows you to fits cox model for given any number of variables, if no varibles, baseline hazard model will be fitted
#' @param train_data data that use to fit the model
#' @param variables names of the variables tat you want to fit the model
#' @return returning a PHM model

#' PHMModelFitting()
#'
#' @export
#'
PHMModelFitting <- function(train_data, variables){

  if(length(variables)== 1){

    fit.breakin <- rms::cph(survival ::Surv(time = start, time2 = end, event = event) ~ get(variables, train_data), data =train_data, surv = T, x=T, y=T)

  }else{

    if(length(variables) == 0){

      fit.breakin <- rms::cph(survival ::Surv(time = start, time2 = end, event = event) ~ 1 , data =train_data, surv = T, x=T, y=T)

    }else{

      f <-as.formula(paste("survival ::Surv(time = start, time2 = end, event = event)" , paste(variables, collapse = " + "), sep = " ~ "))

      fit.breakin <- rms::cph(f , data = train_data, surv = T, x=T, y=T)
    }



  }

  return(fit.breakin)

}
